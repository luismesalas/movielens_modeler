LOG_PATH = "/tmp/"
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
CSV_FILE_PATTERN = "{}.csv"
GENOME_SCORES_NAME = "genome-scores"
GENOME_TAGS_NAME = "genome-tags"
LINKS_NAME = "links"
MOVIES_NAME = "movies"
RATINGS_NAME = "ratings"
TAGS_NAME = "tags"
OUTPUT_FOLDER_DEFAULT_NAME = "movielens_modeler_output"
MOVIELENS_ZIPFILE_NAME = "ml-latest.zip"
MOVIELENS_DOWNLOAD_URL = "http://files.grouplens.org/datasets/movielens/ml-latest.zip"
MOVIELENS_FILES_FOLDER = "ml-latest"
MOVIELENS_REQUIRED_FILES = [CSV_FILE_PATTERN.format(GENOME_SCORES_NAME),
                            CSV_FILE_PATTERN.format(GENOME_TAGS_NAME),
                            CSV_FILE_PATTERN.format(LINKS_NAME),
                            CSV_FILE_PATTERN.format(MOVIES_NAME),
                            CSV_FILE_PATTERN.format(RATINGS_NAME),
                            CSV_FILE_PATTERN.format(TAGS_NAME)]
SPARK_MASTER_URL = "local[1]"
SPARK_APP_NAME = "Movielens modeler local app"
SPARK_PARAMS = {'spark.executor.memory': '8G',
                'spark.driver.memory': '12G'}
