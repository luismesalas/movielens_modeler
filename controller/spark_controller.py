import logging
import math
import time

from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.mllib.recommendation import ALS


class SparkController(object):
    def __init__(self):
        self.__spark_conf = SparkConf().setMaster("local[1]").setAppName("Movielens modeler local app").set(
            'spark.executor.memory', '8G').set('spark.driver.memory', '12G')
        self.__sc = SparkContext(conf=self.__spark_conf)

    def do_als_train_with_params(self, ratings_rdd, iterations, rank, regularization):
        time_start = time.time()
        training_rdd, validation_rdd = ratings_rdd.randomSplit([8, 2], seed=0)
        testing_for_prediction_rdd = validation_rdd.map(lambda x: (x[0], x[1]))
        model = ALS.train(training_rdd, rank, seed=0, iterations=iterations,
                          lambda_=regularization)
        predictions_rdd = model.predictAll(testing_for_prediction_rdd).map(lambda r: ((r[0], r[1]), r[2]))
        rates_and_predictions_rdd = validation_rdd.map(lambda r: ((int(r[0]), int(r[1])), float(r[2]))).join(
            predictions_rdd)
        rsme_error = math.sqrt(rates_and_predictions_rdd.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean())

        time_elapsed = time.time() - time_start

        logging.info("Model trained in {} seconds with a RSME of {}".format(time_elapsed, rsme_error))

        return model

    def do_projection(self, model, userId, movieId):
        return model.predict(userId, movieId);

    # Load ratings.csv file into a PipelineRDD object
    def get_ratings_rdd(self, ratigs_file_path):
        ratings_data = self.__sc.textFile(ratigs_file_path)
        ratings_data_header = ratings_data.take(1)[0]

        # Filter: ignoring header line, split fields and keep only userId, movieId and rating
        ratings_data_rdd = ratings_data.filter(
            lambda line: line != ratings_data_header).map(lambda line: line.split(",")).map(
            lambda tokens: (tokens[0], tokens[1], tokens[2])).cache()

        return ratings_data_rdd

    # Load movies.csv file into a PipelineRDD object
    def get_movies_rdd(self, movies_file_path):
        movies_data = self.__sc.textFile(movies_file_path)
        movies_data_header = movies_data.take(1)[0]

        # Filter: ignoring header line, split fields and keep only movieId and name
        movies_data_rdd = movies_data.filter(lambda line: line != movies_data_header).map(
            lambda line: line.split(",")).map(lambda tokens: (tokens[0], tokens[1])).cache()

        return movies_data_rdd
