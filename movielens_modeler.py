import argparse
import importlib.machinery
import logging
import os
import urllib.request
import zipfile
from datetime import datetime

import types

from controller.spark_controller import SparkController


class MovielensModeler:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        self.__spark_controller = SparkController()
        loader.exec_module(self.__settings)

    def run_movielens_modeler(self, output_path, rank, iterations, regularization):

        try:
            logging.info("Running Movielens Modeler...")

            if not os.path.exists(output_path):
                os.makedirs(output_path)

            # Parameters and some configuration checks
            if not self.get_movielens_dataset(output_path):
                raise Exception("Could not get movielens dataset zip file")

            unzipped_files_path = self.get_movielens_unzipped_files(output_path)
            ratings_file_path = os.path.join(unzipped_files_path, self.__settings.CSV_FILE_PATTERN.format(
                self.__settings.RATINGS_NAME))
            movies_file_path = os.path.join(unzipped_files_path, self.__settings.CSV_FILE_PATTERN.format(
                self.__settings.MOVIES_NAME))

            logging.info("Loading ratings and movies RDD")
            ratings_data_rdd = self.__spark_controller.get_ratings_rdd(ratings_file_path)
            movies_data_rdd = self.__spark_controller.get_movies_rdd(movies_file_path)

            model_file = os.path.join(output_path,
                                      self.__settings.ALS_MODEL_FORMAT_NAME.format(iterations, rank, regularization))

            logging.info(
                "Training model with ALS method and the following params:  Iterations: {}, Rank: {}, Regularization: {}.".format(
                    iterations, rank, regularization))
            model = self.__spark_controller.do_als_train_with_params(ratings_data_rdd, iterations,
                                                                     rank, regularization)

            # Sample prediction
            prediction = self.__spark_controller.do_projection(model, 1, 1)
            logging.info("Sample prediction: user: 1, movie: 1, prediction: {}".format(prediction))

            logging.info("Ratings predictions computed")

        except Exception as ex:
            logging.exception("Exception: {}".format(ex.args))
            exit(-1)

    # Check if ml-latest.zip file exists on path and try to download it if it doesn't exists
    def get_movielens_dataset(self, path):
        if not os.path.exists(os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME)):
            # File does not exist, it tries to download it
            logging.info("Dataset zip file not found in '{}'. Downloading from: '{}'".format(path,
                                                                                             self.__settings.MOVIELENS_DOWNLOAD_URL))
            urllib.request.urlretrieve(self.__settings.MOVIELENS_DOWNLOAD_URL,
                                       os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME))
            # Returning success if file could be downloaded to path
            return os.path.exists(os.path.join(path, self.__settings.MOVIELENS_ZIPFILE_NAME))
        # File already exists
        logging.info("Dataset zip file already exists")
        return True

    # Check if all csv files of ml-latest.zip exists in output_path. If one or more files missing, it tries to unzip ml-latest.zip file
    def get_movielens_unzipped_files(self, output_path):
        unzipped_files_path = os.path.join(output_path, self.__settings.MOVIELENS_FILES_FOLDER)

        if not self.files_required_exists(unzipped_files_path):
            logging.info("Unzipping dataset zip file in '{}'".format(unzipped_files_path))
            # One or more files missing, unzipping ml-latest.zip file
            zip_ref = zipfile.ZipFile(os.path.join(output_path, self.__settings.MOVIELENS_ZIPFILE_NAME), 'r')
            zip_ref.extractall(output_path)
            zip_ref.close()

        if not self.files_required_exists(unzipped_files_path):
            # File couldn't be unzipped, exception raised
            raise Exception("Could not unzip movielens dataset file")
        logging.info("CSV files placed in '{}'".format(unzipped_files_path))
        return unzipped_files_path

    # Check if all csv files contained in ml-latest.zip exists on that path
    def files_required_exists(self, path):
        for file in self.__settings.MOVIELENS_REQUIRED_FILES:
            if not os.path.exists(os.path.join(path, file)):
                return False
        return True


def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO,
                        format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(
        description='Movielens modeler. It uses Alternating Least Squares matrix factorization for creating a prediction model.')
    parser.add_argument('--output_path',
                        help='Path for storing staging and output files. [User home]/movielens_modeler_output by default',
                        required=False,
                        default=os.path.join(os.path.expanduser("~"), settings.OUTPUT_FOLDER_DEFAULT_NAME))
    parser.add_argument('--rank',
                        help='Rank of the feature matrices computed (number of features, default: 15)',
                        required=False,
                        default=15)
    parser.add_argument('--iterations',
                        help='Number of iterations of ALS. (default: 20)',
                        required=False,
                        default=20)
    parser.add_argument('--regularization',
                        help='Regularization parameter.(default: 0.07)',
                        required=False,
                        default=0.07)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting movielens modeler process")
    try:
        movielens_modeler = MovielensModeler("config/settings.py")
        movielens_modeler.run_movielens_modeler(args.output_path, args.rank, args.iterations, args.regularization)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Process finished!")


if __name__ == '__main__':
    main()
