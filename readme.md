# Movielens modeler

This project is built in order to create useful models to predict user affinities based on their rated movies.

We use the [Movielens dataset](https://grouplens.org/datasets/movielens/) for making this models.